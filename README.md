# Probabilistic programming in any language

*An inference server, client libraries and probabilistic program examples.*

This repository is work in progress on probabilistic programming implemented via a library
and a server, rather than through [transformational compilation](https://bitbucket.org/probprog/anglican). 